<?php
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PracticeController;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\FieldsOfPracticeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes([
    'register' => false
]);

Route::resource('practices', PracticeController::class)->middleware([
    'auth'
]);
Route::resource('employees', EmployeeController::class)->middleware([
    'auth'
]);
Route::resource('fields-of-practice', FieldsOfPracticeController::class)->middleware([
    'auth'
]);

Route::delete('delete-practice', [PracticeController::class, 'destroy'])->name("delete-practice");

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
