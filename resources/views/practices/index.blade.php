@extends('layouts.app')

@section('content')
<div class="container">
    <div class="col-md-12 form-group">
        <form method="GET">
            <div class="row form-group">
                <div class="col-md-6">
                    <input type="text" placeholder="Name" class="form-control" name="name" value="<?=isset($_GET["name"]) ? $_GET["name"] : ''?>">
                </div>
            </div>
            <input type="submit" name="search" value="Search" class="btn btn-primary">
            <a href="{{ route('practices.create') }}" class="btn btn-success float-right" role="button" aria-pressed="true">{{ __('Create Practice') }}</a>
        </form>
    </div>

	@if(Session::has('success'))
		<div class="row mb-4 justify-content-center">
			<div class="col-md-12">
	            <div class="alert alert-success" role="alert">
	                {{ Session::get('success') }}
	            </div>
	        </div>
	    </div>
    @elseif(Session::has('error'))
	    <div class="row mb-4 justify-content-center">
			<div class="col-md-12">
	            <div class="alert alert-danger" role="alert">
	                {{ Session::get('error') }}
	            </div>
	        </div>
	    </div>
	@endif

    <div class="row justify-content-left">
        <div class="col-md-12">
            @if('practices')
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">Name</th>
                            <th scope="col">Email</th>
                            <th scope="col">Logo</th>
                            <th scope="col">URL</th>
                            <th scope="col">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($practices as $practice)
                            <tr>
                                <td><a href="{{ route('practices.show', $practice) }}">{{ $practice->name }}</a></td>
                                <td>@if($practice->email){{ $practice->email }}@else-@endif</td>
                                <td>@if($practice->logo) <a target="_blank" href="{{ $practice->logo }}">{{ $practice->logo }}</a>@else-@endif</td>
                                <td>@if($practice->url)<a target="_blank" href="{{ $practice->url }}">{{ $practice->url }}</a>@else-@endif</td>
                                <td>
                                    <a class="btn btn-sm btn-primary float-left" href="{{ route('practices.edit', $practice) }}">Edit</a>
                                    <form id="delete-practice-form" action="{{ route('practices.destroy', $practice) }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button onclick="return confirm('Are you sure?')" type="submit" class="btn btn-sm btn-danger" value="Delete">Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {{ $practices->links() }}
            @else
            	<p class="alert alert-warning">No practices available. Click <a href="{{ route('practices.create') }}">here</a> to create a new practice.</p>
            @endif
        </div>
    </div>
</div>
@endsection
