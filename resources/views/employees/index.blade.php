@extends('layouts.app')

@section('content')
<div class="container">

     <div class="col-md-12 form-group">
        <form method="GET">
            <div class="row form-group">
                <div class="col-md-6">
                    <input type="text" placeholder="Name" class="form-control" name="name" value="<?=isset($_GET["name"]) ? $_GET["name"] : ''?>">
                </div>
            </div>
            <input type="submit" name="search" value="Search" class="btn btn-primary">
            <a href="{{ route('employees.create') }}" class="btn btn-success float-right" role="button" aria-pressed="true">{{ __('Create Employee') }}</a>
        </form>
    </div>


	@if(Session::has('success'))
		<div class="row mb-4 justify-content-center">
			<div class="col-md-12">
	            <div class="alert alert-success" role="alert">
	                {{ Session::get('success') }}
	            </div>
	        </div>
	    </div>
    @elseif(Session::has('error'))
	    <div class="row mb-4 justify-content-center">
			<div class="col-md-12">
	            <div class="alert alert-danger" role="alert">
	                {{ Session::get('error') }}
	            </div>
	        </div>
	    </div>
	@endif

    <div class="row justify-content-left">
        <div class="col-md-12">
            @if('employees')
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">Full Name</th>
                            <th scope="col">Practice</th>
                            <th scope="col">Email</th>
                            <th scope="col">Phone</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($employees as $employee)
                            <tr>
                                <td><a href="{{ route('employees.show', $employee) }}">{{ $employee->full_name }}</a></td>
                                <td>@if($employee->practice_id)<a href="{{ route('practices.show', $employee->practice) }}">{{ $employee->practice->name }}</a>@else-@endif</td>
                                <td>@if($employee->email)<a href="mailto:{{ $employee->email }}">{{ $employee->email }}</a>@else-@endif</td>
                                <td>@if($employee->phone)<a href="tel:{{ $employee->phone }}">{{ $employee->phone }}</a>@else-@endif</td>
                                 <td>
                                    <a class="btn btn-sm btn-primary float-left" href="{{ route('employees.edit', $employee) }}">Edit</a>
                                    <form action="{{ route('employees.destroy', $employee) }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button onclick="return confirm('Are you sure?')" type="submit" class="btn btn-sm btn-danger" value="Delete">Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {{ $employees->links() }}
            @else
            	<p class="alert alert-warning">No employees available. Click <a href="{{ route('employees.create') }}">here</a> to create a new practice.</p>
            @endif
        </div>
    </div>
</div>
@endsection
