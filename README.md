# ![Medical Management System]

[![Build Status](https://img.shields.io/travis/gothinkster/laravel-realworld-example-app/master.svg)]

----------

# Getting started

## Installation

Please check the official laravel installation guide for server requirements before you start. [Official Documentation](https://laravel.com/docs/8.x/installation)

Clone the repository

    git clone https://hariskulas@bitbucket.org/hariskulas/medical-management-system.git

Switch to the repo folder

 	cd your-project

Install all the dependencies using composer

    composer install

Run the database migrations (**Set the database connection in .env before migrating**)

    php artisan migrate
    php artisan db:seed --class=UserSeeder

Start the local development server

    php artisan serve

You can now access the server at http://localhost:8000

**TL;DR command list**

    git clone git@github.com:gothinkster/laravel-realworld-example-app.git
    cd laravel-realworld-example-app
    composer install
    cp .env.example .env
    php artisan key:generate
    
## Application
App url: localhost:8000
Username: administartor@admin.admin
Password: password

## API 
Get all practices URL - localhost:8000/api/practices